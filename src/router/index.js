import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: '首页',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },

  {
    path: '/user',
    component: Layout,
    redirect: '/user/table',
    name: 'HolidayManage',
    meta: { title: '系统管理', icon: 'example' },
    children: [ // 这就是左边的菜单栏
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/holiday/table/index'),
        meta: { title: '用户管理', icon: 'table' }
      },
      {
        path: 'college',
        name: 'College',
        component: () => import('@/views/holiday/college/index'),
        meta: { title: '学院管理', icon: 'table' }
      },
      {
        path: 'class',
        name: 'Class',
        component: () => import('@/views/holiday/class/index'),
        meta: { title: '班级管理', icon: 'table' }
      },
      {
        path: 'role',
        name: 'Role',
        component: () => import('@/views/holiday/role/index'),
        meta: { title: '角色管理', icon: 'table' }
      },
      {
        path: 'log',
        name: 'Log',
        component: () => import('@/views/holiday/log/index'),
        meta: { title: '日志管理', icon: 'table' }
      }
    ]
  },
  {
    path: '/workFlow',
    component: Layout,
    redirect: '/workFlow/holiday',
    name: 'FlowManage',
    meta: { title: '流程管理', icon: 'example' },
    children: [ // 这就是左边的菜单栏
      {
        path: 'workFlow',
        name: 'workFlow',
        component: () => import('@/views/holiday/workFlow/workFlow-link'),
        meta: { title: '流程管理', icon: 'table' }
      },
      {
        path: 'holiday',
        name: 'Holiday',
        component: () => import('@/views/holiday/holiday/holiday-link'),
        meta: { title: '请假单管理', icon: 'table' }
      },
      {
        path: 'task',
        name: 'Task',
        component: () => import('@/views/holiday/task/task-link'),
        meta: { title: '代办任务', icon: 'table' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
