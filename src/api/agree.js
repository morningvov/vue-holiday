import request from '@/utils/request'

const group_name = 'agree'

export default {
  getTotalMonthCount(applyVo) {
    return request({
      url: `/${group_name}/getTotalMonthCount`,
      method: 'post',
      data: applyVo
    })
  },
  getTotalYearCount(applyVo) {
    return request({
      url: `/${group_name}/getTotalYearCount`,
      method: 'post',
      data: applyVo
    })
  },
  getTotalDaysCount(applyVo) {
    return request({
      url: `/${group_name}/getTotalDaysCount`,
      method: 'post',
      data: applyVo
    })
  },
  getTotalWeekCount(applyVo) {
    return request({
      url: `/${group_name}/getTotalWeekCount`,
      method: 'post',
      data: applyVo
    })
  }

}
