import request from '@/utils/request'

const group_name = 'log'

export default {
  logList(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  save(log) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: log
    })
  },
  deleteId(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  update(log) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: log
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
