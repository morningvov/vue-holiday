import request from '@/utils/request'
var group_name = 'holiday'

export default {
  save(holiday) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: holiday
    })
  },
  getByPage(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  update(holiday) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: holiday
    })
  },
  deleteById(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  batchDeleteHoliday(ids) { // 批量删除  ids是数组
    return request({
      url: `/${group_name}/batchDeleteHoliday`,
      method: 'delete',
      data: ids
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
