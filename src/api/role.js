import request from '@/utils/request'

const group_name = 'role'

export default {
  roleList(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  save(role) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: role
    })
  },
  deleteId(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  update(role) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: role
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
