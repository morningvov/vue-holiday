import request from '@/utils/request'

const group_name = 'class'

export default {
  classList(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  save(className) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: className
    })
  },
  deleteId(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  update(className) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: className
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
