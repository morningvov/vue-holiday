import request from '@/utils/request'

const group_name = 'user'

export default {
  tableList(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  save(user) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: user
    })
  },
  deleteId(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  update(user) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: user
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
