import request from '@/utils/request'

var group_name = 'user'

export function login(data) {
  return request({
    url: `/${group_name}/login`,
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: `/${group_name}/info`,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: `/${group_name}/logout`,
    method: 'post'
  })
}

export default {
  save(user) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: user
    })
  },
  getByPage(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  update(user) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: user
    })
  },
  deleteById(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  },
  getNum() { // 获取用户请假次数
    return request({
      url: `/${group_name}/getNum`,
      method: 'get'
    })
  }
}
