import request from '@/utils/request'
var group_name = 'workFlow'

export default {

  loadAllDeployment(page) { // 部署列表查询
    return request({
      url: `/${group_name}/loadAllDeployment`,
      method: 'post',
      data: page
    })
  },

  loadAllProcessDefinition(page) { // 定义列表查询
    return request({
      url: `/${group_name}/loadAllProcessDefinition`,
      method: 'post',
      data: page
    })
  },
  deleteWorkFlow(id) { // 删除
    return request({
      url: `/${group_name}/deleteWorkFlow/${id}`,
      method: 'delete'
    })
  },
  batchDeleteWorkFlow(ids) { // 批量删除  ids是数组
    return request({
      url: `/${group_name}/batchDeleteWorkFlow`,
      method: 'delete',
      data: ids
    })
  },
  viewProcessImage(id) { // 根据id查看流程图
    return request({
      url: `/${group_name}/viewProcessImage/${id}`,
      method: 'get',
      responseType: 'arraybuffer'
    })
  },
  addWorkFlow(fd) { // 上传部署文件
    return request({
      url: `/${group_name}/addWorkFlow`,
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: fd
    })
  },
  startProcess(id) { // 提交启动流程
    return request({
      url: `/${group_name}/startProcess/${id}`,
      method: 'get'
    })
  },
  loadCurrentUserTask(page) { // 查询当前登陆人的代办任务
    return request({
      url: `/${group_name}/loadCurrentUserTask`,
      method: 'post',
      data: page
    })
  },
  toDoTask(id) { // 根据任务id的查询请假信息以及流程选择
    return request({
      url: `/${group_name}/toDoTask/${id}`,
      method: 'get'
    })
  },
  loadAllCommentByTaskId(id) { // 根据任务ID查询批注信息
    return request({
      url: `/${group_name}/loadAllCommentByTaskId/${id}`,
      method: 'get'
    })
  },
  doTask(page) { // 完成任务
    return request({
      url: `/${group_name}/doTask`,
      method: 'post',
      data: page
    })
  },
  toViewProcessByTaskId(id) { // 根据任务ID查看流程进度图
    return request({
      url: `/${group_name}/toViewProcessByTaskId/${id}`,
      method: 'get'
    })
  },
  viewSpProcess(id) { // 根据请假单ID查询审批批注信息和请假单的信息
    return request({
      url: `/${group_name}/viewSpProcess/${id}`,
      method: 'get'
    })
  },
  loadCommentByHolidayId(id) { // 根据请假单的ID查询批注信息
    return request({
      url: `/${group_name}/loadCommentByHolidayId/${id}`,
      method: 'get'
    })
  }
}
