import request from '@/utils/request'

const group_name = 'college'

export default {
  collegeList(page) { // 列表查询
    return request({
      url: `/${group_name}/getByPage`,
      method: 'post',
      data: page
    })
  },
  save(college) { // 添加
    return request({
      url: `/${group_name}/save`,
      method: 'post',
      data: college
    })
  },
  deleteId(id) { // 删除
    return request({
      url: `/${group_name}/delete/${id}`,
      method: 'delete'
    })
  },
  update(college) { // 更新
    return request({
      url: `/${group_name}/update`,
      method: 'put',
      data: college
    })
  },
  get(id) { // 根据id查询
    return request({
      url: `/${group_name}/get/${id}`,
      method: 'get'
    })
  }
}
