import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import md5 from 'js-md5'

// 状态
const getDefaultState = () => {
  return {
    token: getToken(), // token可以从浏览器中的cookie中获取
    name: '',
    avatar: '',
    roleId: 0
  }
}

const state = getDefaultState()

// 改变state的值
const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_ROLEID: (state, roleId) => {
    state.roleId = roleId
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

// 进行异步操作
const actions = {
  // 用户登录
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: md5(password) }).then(res => {
        // 返回的token往vuex中存放
        commit('SET_TOKEN', res.data.token)
        // 这个是往cookie中存放
        setToken(res.data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取用户信息
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getInfo().then(res => {
        const { data } = res
        const { name, header, roleId } = data

        commit('SET_NAME', name)
        commit('SET_ROLEID', roleId)
        commit('SET_AVATAR', header)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 退出登录
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 刷新token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

